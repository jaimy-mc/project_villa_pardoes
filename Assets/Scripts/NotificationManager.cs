﻿using System;
using System.Collections;
using System.Collections.Generic;
using DutchRoseMedia.PlaceablesToolkit.SimpleGestures;
using Unity.Notifications.Android;
using UnityEngine;

public class NotificationManager : MonoBehaviour
{
    private AndroidNotificationChannel mainChannel;
    DateTime notificationFireTime;
    public NotificationTime setTime;
    public DayOfWeek weekDay = DayOfWeek.Monday;
    
    // Start is called before the first frame update
    void Start()
    {
        var notificationIntentData = AndroidNotificationCenter.GetLastNotificationIntent();

        if (notificationIntentData != null)
        {
            /*
            var id = notificationIntentData.Id;
            var channel = notificationIntentData.Channel;
            var notification = notificationIntentData.Notification;
            */
            
            var msg = "Notification received : " + notificationIntentData.Id + "\n";
            msg += "\n Notification received: ";
            msg += "\n .Title: " + notificationIntentData.Notification.Title;
            msg += "\n .Body: " + notificationIntentData.Notification.Text;
            msg += "\n .Channel: " + notificationIntentData.Channel;
            Debug.Log(msg);
        }
    }

    public void TimedNotification()
    {
        int month;
        int dayNumber;

        mainChannel = new AndroidNotificationChannel()
        {
            Id = "push_Notification",
            Name = "Default Channel",
            Importance = Importance.High,
            Description = "Generic notifications",
        };

        //
        if (DayNumber() == false)
        {
            dayNumber = DateTime.Now.Day + 1;
        }
        
        
        
        string day = DayCheck(notificationFireTime);
        if (day == "Tue")
        {
            notificationFireTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month,DateTime.Now.Day,setTime.hour,setTime.min,setTime.sec);

            
            //notification call Dinsdag-dag2
            var notification = new AndroidNotification();
            notification.IntentData = "opened by notification";
            notification.Title = "notificatie dinsdag ";
            notification.Text = "Vandaag zijn er weer Pierres verstopt";
            notification.FireTime = System.DateTime.Now.AddMinutes(1); 

            AndroidNotificationCenter.SendNotification(notification, mainChannel.Id);
        }
        
        //
        if (day == "Wed")
        {
            
        }
        
        //
        if (day == "Thu")
        {
            
        }
    }

    public bool DayNumber()
    {
        int daysInMonth = DateTime.DaysInMonth(DateTime.Now.Year,DateTime.Now.Month);

        if (DateTime.Now.Day + 1 < daysInMonth + 1)
        {
            return true;
        }
        
        return false;
    }

    public string DayCheck(DateTime notificationTime)
    {
        if (weekDay == DayOfWeek.Tuesday)
        {
            return "Tue";
        }
        
        if (weekDay == DayOfWeek.Tuesday)
        {
            return "Wed";
        }
        
        if (weekDay == DayOfWeek.Tuesday)
        {
            return "Thu";
        }

        return "";
    }

    // Update is called once per frame
    void Update()
    {
        //intent when device receives notification get information from it
        
        /*
        AndroidNotificationCenter.NotificationReceivedCallback receivedNotificationHandler = 
            delegate(AndroidNotificationIntentData data)
            {
                var msg = "Notification received : " + data.Id + "\n";
                msg += "\n Notification received: ";
                msg += "\n .Title: " + data.Notification.Title;
                msg += "\n .Body: " + data.Notification.Text;
                msg += "\n .Channel: " + data.Channel;
                Debug.Log(msg);
            };
        AndroidNotificationCenter.OnNotificationReceived += receivedNotificationHandler;
        */
        
        //
    }
}
